const config = require('./config.json')
const haikudos = require('haikudos')
const tmi = require('tmi.js')
const banned_word_list = config.banned_word_list
var Set = require('simple-hashset')

//Timeout username lists
var timeoutSetMinute = new Set()
var timeoutSetHour = new Set()
var timeoutSetDay = new Set('thegreyjaytest')
var banSet = new Set()

// Valid commands start with:
let commandPrefix = '!'
// Define configuration options:
let opts = {
  identity: {
    username: config.settings.bot_username,
    password: 'oauth:' + config.settings.bot_oauth
  },
  channels: [
    config.settings.channel_name
  ]
}

// These are the commands the bot knows (defined below):
let knownCommands = { setup, echo, haiku }

function setup (target, context) {
  sendMessage(target, context, config.messages.setup)
}

// Function called when the "echo" command is issued:
function echo (target, context, params) {
  // If there's something to echo:
  if (params.length) {
    // Join the params into a string:
    const msg = params.join(' ')
    // Send it back to the correct place:
    sendMessage(target, context, msg)
  } else { // Nothing to echo
    console.log(`* Nothing to echo`)
  }
}

// Function called when the "haiku" command is issued:
function haiku (target, context) {
  // Generate a new haiku:
  haikudos((newHaiku) => {
    // Split it line-by-line:
    newHaiku.split('\n').forEach((h) => {
    // Send each line separately:
    sendMessage(target, context, h)
    })
  })
}

// Helper function to send the correct type of message:
function sendMessage (target, context, message) {
  if (context['message-type'] === 'whisper') {
    client.whisper(target, message)
  } else {
    client.say(target, message)
  }
}

function bannedWordHandleUser (target, context, message) {
  if(timeoutSetDay.contains(context.username)){
    banSet.add(context.username)
    timeoutSetDay.remove(context.username)
    client.ban(target, context.username, "ban")
    console.log(`Banned ${context.username} for using word: ${message}`)
  } else if (timeoutSetHour.contains(context.username)) {
    timeoutSetDay.add(context.username)
    timeoutSetHour.remove(context.username)
    client.timeout(target, context.username, 86400, "day")
    console.log(`Timed out ${context.username} for 86400 seconds for using word: ${message}`)
  } else if (timeoutSetMinute.contains(context.username)) {
    timeoutSetHour.add(context.username)
    timeoutSetMinute.remove(context.username)
    client.timeout(target, context.username, 3600, "hour")
    console.log(`Timed out ${context.username} for 3600 seconds for using word: ${message}`)
  } else {
    timeoutSetMinute.add(context.username)
    client.timeout(target, context.username, 60, "minute")
    console.log(`Timed out ${context.username} for 60 seconds for using word: ${message}`)
  }
  console.log(`Ban list: ${banSet.toArray()}`)
  console.log(`Day timeout list: ${timeoutSetDay.toArray()}`)
  console.log(`Hour timeout list: ${timeoutSetHour.toArray()}`)
  console.log(`Minute timeout list: ${timeoutSetMinute.toArray()}`)
}

function messageOk (target, context, message) {
  for (var i = 0; i<banned_word_list.length; i++){
    if(message.toLowerCase().indexOf(banned_word_list[i])>=0){
      bannedWordHandleUser(target, context, banned_word_list[i])
      return false
    }
  }
  return true
}

// Create a client with our options:
let client = new tmi.client(opts)

// Register our event handlers (defined below):
client.on('message', onMessageHandler)
client.on('connected', onConnectedHandler)
client.on('disconnected', onDisconnectedHandler)

// Connect to Twitch:
client.connect()

// Called every time a message comes in:
function onMessageHandler (target, context, msg, self) {
  if (self) { return } // Ignore messages from the bot

  if (!messageOk(target, context, msg)){
    return
  }

  // This isn't a command since it has no prefix:
  if (msg.substr(0, 1) !== commandPrefix) {
    console.log(`[${target} (${context['message-type']})] ${context.username}: ${msg}`)
    return
  }

  // Split the message into individual words:
  const parse = msg.slice(1).split(' ')
  // The command name is the first (0th) one:
  const commandName = parse[0]
  // The rest (if any) are the parameters:
  const params = parse.splice(1)

  // If the command is known, let's execute it:
  if (commandName in knownCommands) {
    // Retrieve the function by its name:
    const command = knownCommands[commandName]
    // Then call the command with parameters:
    command(target, context, params)
    console.log(`* Executed ${commandName} command for ${context.username}`)
  } else {
    console.log(`* Unknown command ${commandName} from ${context.username}`)
  }
}

// Called every time the bot connects to Twitch chat:
function onConnectedHandler (addr, port) {
  console.log(`* Connected to ${addr}:${port}`)
}

// Called every time the bot disconnects from Twitch:
function onDisconnectedHandler (reason) {
  console.log(`Womp womp, disconnected: ${reason}`)
  process.exit(1)
}
